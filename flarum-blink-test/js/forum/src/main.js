/*
app.initializers.add('acme-hello-world', function() {
  alert('Hello, world!');
});
*/
/*
import Post from 'flarum/components/Post';

app.initializers.add('acme-hello-world', function() {
  Post.prototype.view = function() {
    return (
      <div className="Post">
        :D
      </div>
    );
  };
});

import { extend } from 'flarum/extend';
import Post from 'flarum/components/Post';

app.initializers.add('acme-hello-world', function() {
  extend(Post.prototype, 'view', function(vdom) {
    vdom.children.push(':D');
    vdom.attrs.style = 'background-color: yellow';
    console.log(vdom);
  });
});
*/
import { extend } from 'flarum/extend';
import HeaderSecondary from 'flarum/components/HeaderSecondary';

app.initializers.add('acme-hello-world', function() {
  extend(HeaderSecondary.prototype, 'items', function(items) {
    //items.add('google', <a href="http://google.com" className="Button Button--link">Google</a>, 100);
    alert("Forum in here");
  });
});


