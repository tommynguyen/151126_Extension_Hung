System.register('acme/hello-world/main', ['flarum/extend', 'flarum/components/HeaderSecondary'], function (_export) {
  /*
  app.initializers.add('acme-hello-world', function() {
    alert('Hello, world!');
  });
  */
  /*
  import Post from 'flarum/components/Post';
  
  app.initializers.add('acme-hello-world', function() {
    Post.prototype.view = function() {
      return (
        <div className="Post">
          :D
        </div>
      );
    };
  });
  
  import { extend } from 'flarum/extend';
  import Post from 'flarum/components/Post';
  
  app.initializers.add('acme-hello-world', function() {
    extend(Post.prototype, 'view', function(vdom) {
      vdom.children.push(':D');
      vdom.attrs.style = 'background-color: yellow';
      console.log(vdom);
    });
  });
  */
  'use strict';

  var extend, HeaderSecondary;
  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumComponentsHeaderSecondary) {
      HeaderSecondary = _flarumComponentsHeaderSecondary['default'];
    }],
    execute: function () {

      app.initializers.add('acme-hello-world', function () {
        extend(HeaderSecondary.prototype, 'items', function (items) {
          //items.add('google', <a href="http://google.com" className="Button Button--link">Google</a>, 100);
          alert("Forum in here");
        });
      });
    }
  };
});;
System.register('acme/hello-world/test', ['flarum/extend', 'flarum/components/HeaderSecondary'], function (_export) {
  'use strict';

  var extend, HeaderSecondary;
  return {
    setters: [function (_flarumExtend) {
      extend = _flarumExtend.extend;
    }, function (_flarumComponentsHeaderSecondary) {
      HeaderSecondary = _flarumComponentsHeaderSecondary['default'];
    }],
    execute: function () {

      app.initializers.add('acme-hello-world', function () {
        extend(HeaderSecondary.prototype, 'items', function (items) {
          //items.add('google', <a href="http://mp3.zing.vn" className="Button Button--link">MP3</a>, 100);
          alert("Admin in here");
        });
      });
    }
  };
});