<?php

/*
 * This file is part of Flarum.
 *
 * (c) Toby Zerner <toby.zerner@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.


use Flarum\Likes\Listener;
use Illuminate\Contracts\Events\Dispatcher; */
/*
return function (Dispatcher $events) {
    $events->subscribe(Listener\AddClientAssets::class);
    $events->subscribe(Listener\AddPostLikesRelationship::class);
    $events->subscribe(Listener\SaveLikesToDatabase::class);
    $events->subscribe(Listener\SendNotificationWhenPostIsLiked::class);
};
*/
?>
<?php
/*
return function () {
    echo 'Hello, world!';
};
*/

use Flarum\Event\PostWillBeSaved;
use Illuminate\Contracts\Events\Dispatcher;
use Flarum\Event\NotificationWillBeSent;
use Flarum\Event\ConfigureClientView;

return function (Dispatcher $events) {
    /*
    $events->listen(PostWillBeSaved::class, function (PostWillBeSaved $event) {
        $event->post->content = 'This is not what I wrote!';
    });
    */
    $events->listen(ConfigureClientView::class, function (ConfigureClientView $event) {
        if ($event->isForum()) {
            $event->addAssets(__DIR__.'/js/forum/dist/extension.js');
            $event->addBootstrapper('acme/hello-world/main');
        }elseif($event->isAdmin()){
            $event->addAssets(__DIR__.'/js/forum/dist/extension.js');
            $event->addBootstrapper('acme/hello-world/test');
        }
    });
    /*
    $events->listen(NotificationWillBeSent::class, function (NotificationWillBeSent $event) {
        $event->blueprint;
        $event->users;
    });
    */ 
    $events->listen(PostWillBeSaved::class, function (PostWillBeSaved $event) {
        $event->post->edit_user_id = "2";
        $event->post->user_id = "3";
        
    });    
     
};


?>
